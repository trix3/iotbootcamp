//LDR sensor
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(D0, INPUT);
  pinMode(D1, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  int ldrValue = analogRead(D0);

  if (ldrValue > 100) {
    digitalWrite(D1, HIGH);
  }
  else {
    digitalWrite(D1, LOW);
  }
}