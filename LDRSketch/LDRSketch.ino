//LDR sensor
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  clc_rotate(D0, D1);
  delay(2000);
  Serial.println(D0);
  Serial.println(D1);
  // delay(1000);
  // stop(D0, D1);
  // delay(1000);
  // anti_clc_rotate(D1, D0);
  // delay(1000);
}

void stop(int a_pin, int b_pin){
  digitalWrite(a_pin, LOW);
  digitalWrite(b_pin, LOW);
}

void rotate(int a_pin, int b_pin) {
  digitalWrite(a_pin, LOW);
  digitalWrite(b_pin, HIGH);
}

void anti_clc_rotate(int b_pin, int a_pin) {
  rotate(b_pin, a_pin);
}

void clc_rotate(int a_pin, int b_pin) {
  rotate(a_pin, b_pin);
}
