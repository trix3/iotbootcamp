#define sound_speed_in_air 0.034 //in cm per micro seconds
const int trigger = D0;
const int echo = D1;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
  pinMode(D2, OUTPUT);
  pinMode(D4, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  //clear trigger
  digitalWrite(trigger, LOW);
  delayMicroseconds(2);

  //clear LEDs
  digitalWrite(D4, LOW);

  // clock cycle
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);

  float duration = pulseIn(echo, HIGH);
  // Serial.println("Duration " + (String) duration);
  float distance = duration * sound_speed_in_air / 2;
  Serial.println("Distance " + (String) distance);
  if (distance < 30) {
    digitalWrite(D4, HIGH);
  }
  delay(1000);
  

}
